Client-side TF2 mod that replaces the default map loading screens with 1920x1080 screenshots of the map. Should work well for anyone using a 16:9 aspect ratio 1920x1080 or smaller.

**IMPORTANT NOTICE:** The Matchmaking update broke this. I don't have plans to fix it anytime soon.

## Installation
Extract or clone this repository into your tf/custom/ folder.

If you are using a custom HUD, navigate to tf/custom/custom HUD folder/resource/ui/ and rename "statsummary.res" to "statsummary.res.bak". You will likely need to do this every time you update your HUD.

## Notes
- This hasn't been tested extensively, and might not work with some HUDs. 
- I haven't updated any of these maps since the _End of the Line_ update. Loading any maps from the _Gun Mettle_ update or later will show your HUD's default loading screen or background.
